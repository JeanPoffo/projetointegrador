package Controller.View;

import javax.swing.JButton;

/**
 * Classe View Main
 * @author Jean Poffo
 */
public class View extends javax.swing.JFrame {

    private javax.swing.JButton botaoAdicionar;
    
    private javax.swing.JButton botaoDefinirTamanho;
    
    private javax.swing.JButton botaoOrdenarPallet;
    
    private javax.swing.JButton botaoRemover;
    
    private javax.swing.JList<String> listPallet;
    
    private javax.swing.JScrollPane scroll;
    
    private javax.swing.JPanel panelArmazem;
    
    private javax.swing.JPanel panelContainerArmazem;
    
    private javax.swing.JPanel panelPallets;          

    public JButton getBotaoAdicionar() {
        return botaoAdicionar;
    }

    public JButton getBotaoDefinirTamanho() {
        return botaoDefinirTamanho;
    }

    public JButton getBotaoOrdenarPallet() {
        return botaoOrdenarPallet;
    }

    public JButton getBotaoRemover() {
        return botaoRemover;
    }
    
    /**
     * Construtor
     */
    public View() {
        initComponents();
    }
                         
    private void initComponents() {
        panelPallets          = new javax.swing.JPanel();
        botaoAdicionar        = new javax.swing.JButton();
        botaoRemover          = new javax.swing.JButton();
        panelContainerArmazem = new javax.swing.JPanel();
        panelArmazem          = new javax.swing.JPanel();
        botaoDefinirTamanho   = new javax.swing.JButton();
        botaoOrdenarPallet    = new javax.swing.JButton();
        scroll                = new javax.swing.JScrollPane();
        listPallet            = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelPallets.setBorder(javax.swing.BorderFactory.createTitledBorder("Pallets"));
        panelPallets.setToolTipText("");

        listPallet.setModel(new PalletListModel());
        scroll.setViewportView(listPallet);

        botaoAdicionar.setText("Adicionar");

        botaoRemover.setText("Remover");

        javax.swing.GroupLayout panelPalletsLayout = new javax.swing.GroupLayout(panelPallets);
        panelPallets.setLayout(panelPalletsLayout);
        panelPalletsLayout.setHorizontalGroup(
            panelPalletsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPalletsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPalletsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scroll)
                    .addGroup(panelPalletsLayout.createSequentialGroup()
                        .addComponent(botaoAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoRemover, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelPalletsLayout.setVerticalGroup(
            panelPalletsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPalletsLayout.createSequentialGroup()
                .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPalletsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoAdicionar)
                    .addComponent(botaoRemover))
                .addContainerGap())
        );

        panelContainerArmazem.setBorder(javax.swing.BorderFactory.createTitledBorder("Armazém"));

        panelArmazem.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout panelArmazemLayout = new javax.swing.GroupLayout(panelArmazem);
        panelArmazem.setLayout(panelArmazemLayout);
        panelArmazemLayout.setHorizontalGroup(
            panelArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelArmazemLayout.setVerticalGroup(
            panelArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        botaoDefinirTamanho.setText("Definir Tamanho");

        botaoOrdenarPallet.setText("Ordenar Pallet");

        javax.swing.GroupLayout panelContainerArmazemLayout = new javax.swing.GroupLayout(panelContainerArmazem);
        panelContainerArmazem.setLayout(panelContainerArmazemLayout);
        panelContainerArmazemLayout.setHorizontalGroup(
            panelContainerArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelContainerArmazemLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelContainerArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelArmazem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelContainerArmazemLayout.createSequentialGroup()
                        .addComponent(botaoOrdenarPallet, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 139, Short.MAX_VALUE)
                        .addComponent(botaoDefinirTamanho)))
                .addContainerGap())
        );
        panelContainerArmazemLayout.setVerticalGroup(
            panelContainerArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelContainerArmazemLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelArmazem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelContainerArmazemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botaoDefinirTamanho)
                    .addComponent(botaoOrdenarPallet))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPallets, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelContainerArmazem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelContainerArmazem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPallets, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }
}