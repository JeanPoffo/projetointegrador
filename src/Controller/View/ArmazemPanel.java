/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.View;

import Controller.Model.Pallet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import javax.swing.JPanel;

/**
 *
 * @author mathe
 */
public class ArmazemPanel extends JPanel {

    private List<Pallet> pallets;
    private int tamanho;

    public List<Pallet> getPallets() {
        return pallets;
    }

    public void setPallets(List<Pallet> pallets) {
        this.pallets = pallets;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public ArmazemPanel(List<Pallet> pallets) {
        this.pallets = pallets;
        this.tamanho = tamanho;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        ordenarPorLargura();
        int x = 0;
        int y = 0;
        int prevLargura = 0;
        int prevAltura = 0;
        int proxLargura = 0;
        int proxAltura = 0;
        int coef = 1;
        
        g.drawString(" Armazém", tamanho, tamanho/2);
        g.drawRect(0, 0, this.tamanho, this.tamanho);

        for (int i = 0; i < pallets.size(); i++) {
            Pallet pallet = pallets.get(i);
            
            int largura = pallet.getLargura() * pallet.getLargura();
            int altura = pallet.getAltura() * pallet.getAltura();
            
            if (i-1 >= 0){
                prevLargura = pallets.get(i  -1).getLargura() * pallets.get(i - 1).getLargura();
                prevAltura = pallets.get(i  -1).getAltura() * pallets.get(i - 1).getAltura();
            }
            
            if (i+1 < pallets.size()){
                proxLargura = pallets.get(i  +1).getLargura() * pallets.get(i + 1).getLargura();
                proxAltura = pallets.get(i  +1).getAltura() * pallets.get(i + 1).getAltura();
            }
            altura *= coef;
            if (y + altura <= tamanho && y + altura > 0)
            {
                if (altura < 0){
                    //g.setColor(this.randomColor());
                    y+= altura;
                    g.drawRect(x, y, largura, altura*-1);
                }
                else{
                    g.drawRect(x, y, largura, altura);
                    y+= altura;
                }
                System.out.println("//if");
                System.out.print(y +"+"+altura +" = ");
                //y += altura;
                System.out.print (y);
                System.out.println(" x = "+x);
            }
            else
            {
                // altura é nagativa?
                if (altura < 0)
                {
                   y = 0;
                }
                else
                {
                    y = tamanho - altura;
                }
                x += prevLargura;
                coef *= -1;
                System.out.println("//else");
                System.out.print(y +"+"+altura +" = ");
                System.out.print (y);
                System.out.println(" x = "+x);
                if (altura < 0){
                    //g.setColor(this.randomColor());
                    g.drawRect(x, y, largura, altura*-1);
                }
                else{
                    g.drawRect(x, y, largura, altura);
                }
            }
            //System.out.println("y: " +y + " altura: "+ altura);
        }
    }

    void ordenarPorLargura() {
        this.pallets.sort((Pallet p1, Pallet p2) -> {
            if (p1.getLargura() > p2.getLargura()) {
                return 1;
            } else if (p1.getLargura() < p2.getLargura()) {
                return -1;
            }
            return 0;
        });
    }
    
    Color randomColor(){
        Random r = new Random(); 
        return new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255));
    }
}
