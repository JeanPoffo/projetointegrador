package Controller.View;

import Controller.ControllerMain;
import javax.swing.AbstractListModel;

/**
 * Classe de Modelo Do JTable
 * @author Jean Poffo
 */
public class PalletListModel extends AbstractListModel {

    private ControllerMain controller;

    public PalletListModel() {
        this.controller = ControllerMain.getInstance();
    }
    
    @Override
    public int getSize() {
        return this.controller.getPallet().size();
    }

    @Override
    public String getElementAt(int index) {
        return this.controller.getPallet().get(index).getNome();
    }
}