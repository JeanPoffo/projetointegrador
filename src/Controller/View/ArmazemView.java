
package Controller.View;

import Controller.Model.Pallet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;
import javax.swing.JFrame;


public class ArmazemView extends JFrame{
  
    private ArmazemPanel ap;
    private int tamanho;
    
    public ArmazemView(int tamanho, ArmazemPanel ap){
        this.setResizable(false);
        this.ap = ap;
        ap.setTamanho(tamanho);
        this.add(ap);
        this.tamanho  = tamanho;
        Dimension d = new Dimension(this.tamanho+200,this.tamanho+200);
        this.setSize(d);
        this.setVisible(true);
    }
}
