package Controller;

import Controller.Model.Pallet;
import Controller.View.ArmazemPanel;
import Controller.View.ArmazemView;
import Controller.View.View;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Classe Main do Sistema
 * @author Jean Poffo
 */
public class ControllerMain {
    
    private View view;
    
    private ArmazemView armazemView;
    
    private static ControllerMain instance;

    private List<Pallet> pallet;

    private int tamanhoArmazem;
    
    
    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public int getTamanhoArmazem() {
        return tamanhoArmazem;
    }

    public void setTamanhoArmazem(int tamanhoArmazem) {
        this.tamanhoArmazem = tamanhoArmazem;
    }
    
    public List<Pallet> getPallet() {
        return pallet;
    }
    
    public void addPallet(Pallet pallet) {
        this.getPallet().add(pallet);
    }
    
    public void init() {
        this.pallet = new ArrayList();
        this.view   = new View();
        
        this.addListeners();
        this.view.setVisible(true);
    }
    
    public static ControllerMain getInstance() {
        if(instance == null) {
            instance = new ControllerMain();
        }
        
        return instance;
    } 
    
    private void addListeners() {
        this.getView().getBotaoAdicionar().addActionListener((ActionEvent e) -> {
            System.out.println("Clicou Botao Adicionar");
        });
        
        this.getView().getBotaoRemover().addActionListener((ActionEvent e) -> {
            System.out.println("Clicou Botao Remover");
        });
        
        this.getView().getBotaoOrdenarPallet().addActionListener((ActionEvent e) -> {
            ControllerMain.this.desenhaPalletArmazem();
            System.out.println("Desenhou Pallets");
        });
        
        this.getView().getBotaoDefinirTamanho().addActionListener((ActionEvent e) -> {
            ControllerMain.this.tamanhoArmazem = Integer.parseInt(JOptionPane.showInputDialog(ControllerMain.this.view, "Digite o tamanho do armazém:"));
            System.out.println("Tamanho Armazem: " + this.tamanhoArmazem);
        });
    }
    
    private void desenhaPalletArmazem() {
        List<Pallet> palletOrdenado = this.getPalletsOrdenado(this.pallet, this.tamanhoArmazem);
        armazemView = new ArmazemView(tamanhoArmazem, new ArmazemPanel(palletOrdenado));
    }
    
    /**
     * 
     * @param listPallet
     * @param tamanhoArmazem
     * @return List - Lista Ordenada
     */
    public List<Pallet> getPalletsOrdenado(List<Pallet> listPallet, int tamanhoArmazem) {
        /** Quantidade de Pallets */
        int quantidadeItem = listPallet.size();
        /** Monta uma Matriz que armazena os Tamanhos do Pallet x Tamanho Armazem */
        int [][] tamanhoItemPallet = new int[quantidadeItem + 1][tamanhoArmazem + 1];
        for (int item = 1; item <= quantidadeItem; item++) {
            /** Preenche a Matriz com os valores */
            for (int tamanhoQuadrado = 1; tamanhoQuadrado <= tamanhoArmazem; tamanhoQuadrado++ ) { 
                /** Verifica se o Pallet possui um tamanho menor ou igual ao Tamanho do Armazem */
                if (listPallet.get(item - 1).getTamanhoQuadrado() <= tamanhoQuadrado) {
                    /** Retorn o maximo da soma do Pallet, do Item atual e o valor da coluna */
                    tamanhoItemPallet[item][tamanhoQuadrado] = Math.max(listPallet.get(item - 1).getPreco() + tamanhoItemPallet[item - 1][tamanhoQuadrado - listPallet.get(item - 1).getTamanhoQuadrado()], tamanhoItemPallet[item - 1][tamanhoQuadrado]);
                }
                else {
                    /** Caso nao couber, preenche com o item anterior */
                    tamanhoItemPallet [item][tamanhoQuadrado] = tamanhoItemPallet[item - 1][tamanhoQuadrado];
                }
            }
        }
        /** Verifica e ordena os itens escolhidos */
        int colunaMetroQuadrado             = tamanhoArmazem;
        ArrayList<Pallet> itensSelecionados = new ArrayList<>(); 
        for (int item = quantidadeItem; item >= 1; item--) {
            if (tamanhoItemPallet [item][colunaMetroQuadrado] != tamanhoItemPallet [item - 1][colunaMetroQuadrado]){
                itensSelecionados.add(listPallet.get(item - 1));        
                colunaMetroQuadrado -= listPallet.get(item - 1).getTamanhoQuadrado();
            }
        }
        System.out.println("Produtos escolhidos: " + itensSelecionados);
        System.out.println("Valor total dos itens no Armazem: " + tamanhoItemPallet[quantidadeItem][tamanhoArmazem]);
        return itensSelecionados;
    }
}
