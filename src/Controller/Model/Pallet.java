package Controller.Model;

/**
 * Classe de Modelo do Pallet
 * @author Jean Poffo
 */
public class Pallet {

    private String nome;
    
    private int altura;
    
    private int largura;
    
    private int preco;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }
    
    public int getTamanhoQuadrado() {
        return (this.altura * this.largura);
    }
    
    public float getPrecoPorMetroQuadrado() {
        return (this.preco / this.getTamanhoQuadrado());
    }

    @Override
    public String toString() {
        return this.getNome();
    }
}