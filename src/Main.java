import Controller.ControllerMain;
import Controller.Model.Pallet;

/**
 * Main
 * @author Jean Poffo
 */
public class Main {
    
    public static void main(String[] args) {
        Pallet a = new Pallet();
        Pallet e = new Pallet();
        Pallet i = new Pallet();
        Pallet o = new Pallet();
        Pallet u = new Pallet();
        
        a.setNome("Coca");
        a.setAltura(5);
        a.setLargura(3);
        a.setPreco(15);
        
        e.setNome("Amendoin");
        e.setAltura(5);
        e.setLargura(5);
        e.setPreco(1);
        
        i.setNome("Cerveja");
        i.setAltura(3);
        i.setLargura(6);
        i.setPreco(13);
        
        o.setNome("Vinho");
        o.setAltura(6);
        o.setLargura(4);
        o.setPreco(14);
        
        u.setNome("Batata");
        u.setAltura(7);
        u.setLargura(7);
        u.setPreco(30);
        
        ControllerMain.getInstance().init();
        ControllerMain.getInstance().addPallet(a);
        ControllerMain.getInstance().addPallet(e);
        ControllerMain.getInstance().addPallet(i);
        ControllerMain.getInstance().addPallet(o);
        ControllerMain.getInstance().addPallet(u);
    }
}